package de.quinscape.intrexx.wellcarton.bizsync;

import de.quinscape.intrexx.db.tester.AbstractSpringBatchTest;
import de.quinscape.intrexx.db.tester.annotations.ExpectedDataSets;
import de.quinscape.intrexx.db.tester.annotations.InitialDataSets;
import de.quinscape.intrexx.db.tester.annotations.SpringContext;
import de.quinscape.intrexx.db.tester.annotations.Truncations;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;




























@SpringContext({"cfg/spring/context.xml", "cfg/spring/db.xml"})
public class StaxEventItemReaderFunctionalTest
  extends AbstractSpringBatchTest
{
  private static final String FILE_KALKULATION_1 = "kalkulation_1.XML";
  
  @Test
  @Truncations({"datasource.portal:MAV2_MUSTERAUFTRAG"})
  @ExpectedDataSets({"StaxEventItemReader_readOneAndXxx_expected"})
  @InitialDataSets({"StaxEventItemReader_readOneAndInsert_initial"})
  public void readOneAndInsert() {
    FileTools.prepareDataFiles(



        
        new String[] { "kalkulation_1.XML" });
    Map<String, JobParameter> parametersMap = new HashMap<String, JobParameter>();
    parametersMap.put("filename", new JobParameter("../../test/functional/data/kalkulation_1.XML"));
    runJob("mas2Import", ExitStatus.COMPLETED, new JobParameters(parametersMap));
    checkExpectations();
  }

  
  @Test
  @Truncations({"datasource.portal:MAV2_MUSTERAUFTRAG"})
  @ExpectedDataSets({"StaxEventItemReader_readOneAndXxx_expected"})
  @InitialDataSets({"StaxEventItemReader_readOneAndUpdate_initial"})
  public void readOneAndUpdate() {
    FileTools.prepareDataFiles(



        
        new String[] { "kalkulation_1.XML" });
    Map<String, JobParameter> parametersMap = new HashMap<String, JobParameter>();
    parametersMap.put("filename", new JobParameter("../../test/functional/data/kalkulation_1.XML"));
    runJob("mas2Import", ExitStatus.COMPLETED, new JobParameters(parametersMap));
    checkExpectations();
  }
}
