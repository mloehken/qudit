package de.quinscape.intrexx.wellcarton.bizsync;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class ImportLauncher {
	 
	private static final Log log = LogFactory.getLog(ImportLauncher.class);
	
	private static final String JOB_LAUNCHER_BEAN_NAME = "jobLauncher";

    private static final String JOB_REGISTRY_BEAN_NAME = "jobRegistry";

    private static class AcceptXmlFilesFilenameFilter
    implements FilenameFilter
  {
    public boolean accept(File directory, String name) { return name.toLowerCase().endsWith(".xml"); }
  }

	public static void main(String[] args) throws Exception {
	    ImportLauncher launcher = new ImportLauncher();
	    Properties properties = new Properties();
	    properties.load(new BufferedReader(new FileReader("cfg/bizsync.properties")));
	    launcher.setContextPath(properties.getProperty("importLauncher.contextPath"));
	    launcher.setDirectoryPath(properties.getProperty("importLauncher.directoryPath"));
	    launcher.setJobName("mas2Import");
	    launcher.run();
	  }
	
	
	 private String contextPath = "cfg/spring/context.xml";

	  
	  private File destDirFailed;

	  
	  private File destDirSuccessful;

	  
	  private File dir;

	  
	  private String jobName;

	  
	  public void setContextPath(String contextPath) { this.contextPath = contextPath; }

	  void run() {
		    String[] filenames = this.dir.list(new AcceptXmlFilesFilenameFilter());
		    Arrays.sort(filenames); byte b; int i; String[] arrayOfString;
		    for (i = arrayOfString = filenames.length, b = 0; b < i; ) { String filename = arrayOfString[b];
		      
		      File file = new File(this.dir, filename);
		      FileSystemXmlApplicationContext fileSystemXmlApplicationContext = new FileSystemXmlApplicationContext(this.contextPath);
		      JobRegistry registry = (JobRegistry)fileSystemXmlApplicationContext.getBean("jobRegistry");
		      Job job = registry.getJob(this.jobName);
		      JobLauncher launcher = (JobLauncher)fileSystemXmlApplicationContext.getBean("jobLauncher");
		      Map<String, JobParameter> parametersMap = new HashMap<String, JobParameter>();
		      log.debug("Start to process file: " + file.getAbsolutePath());
		      parametersMap.put("filename", new JobParameter(file.getAbsolutePath()));
		      JobExecution execution = launcher.run(job, new JobParameters(parametersMap));
		      waitForCompletion(execution);
		      moveInputFile(file, execution);
		      log.debug("Ended to process file: " + file.getAbsolutePath());
		      b++; }
		  
		  }
		  
		  private void moveInputFile(File inputFile, JobExecution execution) {
		    File targetFile;
		    if (jobWasCompleted(execution)) {
		      
		      targetFile = new File(this.destDirSuccessful, inputFile.getName());
		      log.debug("job was completed; renaming file to: " + targetFile);
		    }
		    else {
		      
		      targetFile = new File(this.destDirFailed, inputFile.getName());
		      log.debug("job was NOT completed; renaming file to: " + targetFile);
		    } 
		    if (targetFile.exists()) targetFile.delete(); 
		    inputFile.renameTo(targetFile);
		  }














		  
		  void setDirectoryPath(String path) {
		    File newDir = new File(path);
		    if (!newDir.isDirectory())
		      throw new FileNotFoundException("'" + newDir.getAbsolutePath() + "' is no directory."); 
		    this.dir = newDir;
		    this.destDirSuccessful = new File(this.dir, "processed");
		    this.destDirFailed = new File(this.dir, "failed");
		  }








		  
		  void setJobName(String jobName) { this.jobName = jobName; }



		  
		  private boolean jobWasCompleted(JobExecution execution) { return execution.getExitStatus().getExitCode().equals(ExitStatus.COMPLETED.getExitCode()); }


		  
		  private void waitForCompletion(JobExecution execution) {
		    while (execution.getExitStatus().getExitCode().equals(ExitStatus.UNKNOWN.getExitCode())) {

		      
		      try {
		        Thread.sleep(100L);
		      }
		      catch (InterruptedException interruptedException) {}
		    } 
		  }
}
