package de.quinscape.intrexx.wellcarton.bizsync;

import java.io.File;
import org.junit.Assert;
import org.junit.Test;




























public class ImportLauncherTest
{
  @Test
  public void testSuccessfulLaunch() {
    FileTools.prepareDataFiles(





        
        new String[] { "kalkulation_1.XML" });
    ImportLauncher.main(new String[] { "cfg/spring/context.xml", "../../test/functional/data" });
    Assert.assertFalse((new File("../../test/functional/data/kalkulation_1.XML")).exists());
    Assert.assertTrue((new File("../../test/functional/data/processed/kalkulation_1.XML")).exists());
    Assert.assertFalse((new File("../../test/functional/data/failed/kalkulation_1.XML")).exists());
  }



  
  @Test
  public void testFailingLaunch() {
    FileTools.prepareDataFiles(





        
        new String[] { "kalkulation_1-failing.XML" });
    ImportLauncher.main(new String[] { "cfg/spring/context.xml", "../../test/functional/data" });
    Assert.assertFalse((new File("../../test/functional/data/kalkulation_1-failing.XML")).exists());
    Assert.assertFalse((new File("../../test/functional/data/processed/kalkulation_1-failing.XML")).exists());
    Assert.assertTrue((new File("../../test/functional/data/failed/kalkulation_1-failing.XML")).exists());
  }
}
