package de.quinscape.intrexx.wellcarton.bizsync;

import de.quinscape.intrexx.db.tester.AbstractSpringBatchTest;
import de.quinscape.intrexx.db.tester.annotations.ExpectedDataSets;
import de.quinscape.intrexx.db.tester.annotations.InitialDataSets;
import de.quinscape.intrexx.db.tester.annotations.SpringContext;
import de.quinscape.intrexx.db.tester.annotations.Truncations;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;






@SpringContext({"cfg/spring/context.xml", "cfg/spring/db-intrexx.xml", "cfg/spring/db-abstract.xml"})
public class ImportClassifierTest
  extends AbstractSpringBatchTest
{
  private static final String KALK_TWO = "Copy of kalkulation_1020120_20120726091240.XML";
  private static final String KALK_ONE = "kalkulation_1020120_20120726091240.XML";
  
  @Test
  @Truncations({"datasource.portal:MAV2_MUSTERAUFTRAG"})
  @InitialDataSets({"ImportClassifierTest_classify_initial"})
  @ExpectedDataSets({"ImportClassifierTest_classify_expected"})
  public void classify() {
    Map<String, JobParameter> parametersMap = new HashMap<String, JobParameter>();
    FileTools.prepareDataFiles(new String[] { "kalkulation_1020120_20120726091240.XML", "Copy of kalkulation_1020120_20120726091240.XML" });
    
    parametersMap.put("filename", 
        new JobParameter(FileTools.getAbsoluteInputPath("kalkulation_1020120_20120726091240.XML")));
    runJob("mas2Import", ExitStatus.COMPLETED, new JobParameters(parametersMap));
    Assert.assertFalse(FileTools.isProcessed("kalkulation_1020120_20120726091240.XML"));
    Assert.assertTrue(FileTools.isStillToBeProcessed("kalkulation_1020120_20120726091240.XML"));
    
    parametersMap.put("filename", new JobParameter(FileTools.getAbsoluteInputPath("Copy of kalkulation_1020120_20120726091240.XML")));
    runJob("mas2Import", ExitStatus.COMPLETED, new JobParameters(parametersMap));
    
    checkExpectations();
  }
}
