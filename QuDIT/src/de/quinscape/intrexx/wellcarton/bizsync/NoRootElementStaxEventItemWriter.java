package de.quinscape.intrexx.wellcarton.bizsync;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import org.springframework.batch.item.xml.StaxEventItemWriter;










public class NoRootElementStaxEventItemWriter<T>
  extends StaxEventItemWriter<T>
{
  protected void startDocument(XMLEventWriter writer) throws XMLStreamException {
    XMLEventFactory factory = XMLEventFactory.newInstance();
    writer.add(factory.createStartDocument(getEncoding(), getVersion()));



    
    writer.add(factory.createIgnorableSpace(""));
    writer.flush();
  }
  
  protected void endDocument(XMLEventWriter writer) throws XMLStreamException {}
}
