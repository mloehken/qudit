package de.quinscape.intrexx.wellcarton.bizsync;

import org.springframework.batch.item.file.ResourceSuffixCreator;
import org.springframework.beans.factory.annotation.Required;

























public class TimestampSuffixCreator
  implements ResourceSuffixCreator
{
  private String extension;
  private String prefix;
  
  public String getExtension() { return this.extension; }








  
  public String getPrefix() { return this.prefix; }



  
  public String getSuffix(int arg0) {
    long millis = System.currentTimeMillis();
    return String.valueOf(this.prefix) + millis + this.extension;
  }









  
  @Required
  public void setExtension(String extension) { this.extension = extension; }










  
  @Required
  public void setPrefix(String prefix) { this.prefix = prefix; }
}
