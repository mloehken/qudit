package de.quinscape.intrexx.wellcarton.bizsync;


import java.io.File;
import java.io.FileNotFoundException;
import org.apache.commons.io.FileUtils;
import org.hamcrest.Matchers;
import org.junit.Assert;






















public class FileTools
{
  static final File filesInput = new File("../../test/functional/data");




  
  static final File filesProcessed = new File(filesInput, "processed");



  
  static final File filesStore = new File(filesInput, "store");












  
  static void assertDataFiles(String... fileNames) {
    byte b;
    int i;
    String[] arrayOfString;
    for (i = arrayOfString = fileNames.length, b = 0; b < i; ) { String fName = arrayOfString[b];
      
      File processedFile = new File(filesProcessed, fName);
      File testFile = new File(filesInput, fName);
      Assert.assertThat(Boolean.valueOf(processedFile.exists()), Matchers.is(Boolean.valueOf(true)));
      Assert.assertThat(Boolean.valueOf(testFile.exists()), Matchers.is(Boolean.valueOf(false)));
      b++; }
  
  }












  
  static void prepareDataFiles(String... fileNames) {
    byte b;
    int i;
    String[] arrayOfString;
    for (i = arrayOfString = fileNames.length, b = 0; b < i; ) { String fName = arrayOfString[b];
      
      File storedFile = new File(filesStore, fName);
      File processedFile = new File(filesProcessed, fName);
      File testFile = new File(filesInput, fName);
      processedFile.delete();
      if (!storedFile.exists())
        throw new FileNotFoundException(storedFile.getAbsolutePath()); 
      FileUtils.copyFile(storedFile, testFile);
      b++; }
  
  }





  
  public static boolean isProcessed(String filename) { return (new File(filesProcessed, filename)).exists(); }






  
  static boolean isStillToBeProcessed(String filename) { return (new File(filesInput, filename)).exists(); }







  
  static String getAbsoluteInputPath(String filename) { return (new File(filesInput, filename)).getAbsolutePath(); }
}
