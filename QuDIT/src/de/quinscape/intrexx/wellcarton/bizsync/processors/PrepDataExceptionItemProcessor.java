package de.quinscape.intrexx.wellcarton.bizsync.processors;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemProcessor;













public class PrepDateExceptionItemProcessor
  extends Object
  implements ItemProcessor<Map<String, Object>, Map<String, Object>>
{
  private static final Log log = LogFactory.getLog(PrepDateExceptionItemProcessor.class);









  
  public Map<String, Object> process(Map<String, Object> item) throws Exception {
    Calendar cal1 = new GregorianCalendar('��', false, 10);
    Date date1 = cal1.getTime();
    Calendar cal2 = new GregorianCalendar('���', 11, 29);
    Date date2 = cal2.getTime();
    Date datumBis = (Date)item.get("AUFT_LT_DATUM_BIS");
    
    if (!datumBis.before(date1) || !datumBis.after(date2)) {
      
      log.error("Nur Datumsangaben zwischen 1.1.1753 31.12.9999 sind erlaubt: " + item);
      item.put("AUFT_LT_DATUM_BIS", null);
    } 
    item.put("EK_KEY", Integer.valueOf(((Double)item.get("EK_KEY")).intValue()));
    item.put("EK_POS_NR", Integer.valueOf(((Double)item.get("EK_POS_NR")).intValue()));
    
    return item;
  }
}
