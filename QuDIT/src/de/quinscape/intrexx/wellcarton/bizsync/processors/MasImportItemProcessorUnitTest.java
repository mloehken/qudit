package de.quinscape.intrexx.wellcarton.bizsync.processors;

import de.quinscape.intrexx.wellcarton.bizsync.domain.Kalkulation;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;



















public class MasImportItemProcessorUnitTest
{
  private static class MyKalk
    extends Kalkulation
  {
    private String[] materialGruppen;
    private int currentIndex = 0;


    
    MyKalk(String... materialGruppen) { this.materialGruppen = materialGruppen; }









    
    public String getMaterialGruppe() { return this.materialGruppen[this.currentIndex % this.materialGruppen.length]; }



    
    public void advance() { this.currentIndex++; }
  }








  
  @Test
  public void bestimmeGruppe() {
    MyKalk kalk = new MyKalk(new String[] { "A61", "A62", "A64", "A68", "A71", "A73", "A74", "A76", "A31", "A34", "A33", "A36", "A39", "A63", "A65", "A66", "A69" });


    
    MasImportItemProcessor processor = new MasImportItemProcessor();
    Assert.assertThat(processor.bestimmeBereich(kalk), Is.is(Integer.valueOf(1)));
    kalk.advance();
    Assert.assertThat(processor.bestimmeBereich(kalk), Is.is(Integer.valueOf(1)));
    kalk.advance();
    Assert.assertThat(processor.bestimmeBereich(kalk), Is.is(Integer.valueOf(1)));
    kalk.advance();
    Assert.assertThat(processor.bestimmeBereich(kalk), Is.is(Integer.valueOf(1)));
    kalk.advance();
    Assert.assertThat(processor.bestimmeBereich(kalk), Is.is(Integer.valueOf(1)));
    kalk.advance();
    Assert.assertThat(processor.bestimmeBereich(kalk), Is.is(Integer.valueOf(1)));
    kalk.advance();
    Assert.assertThat(processor.bestimmeBereich(kalk), Is.is(Integer.valueOf(1)));
    kalk.advance();
    Assert.assertThat(processor.bestimmeBereich(kalk), Is.is(Integer.valueOf(1)));
    kalk.advance();
    Assert.assertThat(processor.bestimmeBereich(kalk), Is.is(Integer.valueOf(1)));
    kalk.advance();
    Assert.assertThat(processor.bestimmeBereich(kalk), Is.is(Integer.valueOf(1)));
    kalk.advance();
    Assert.assertThat(processor.bestimmeBereich(kalk), Is.is(Integer.valueOf(2)));
    kalk.advance();
    Assert.assertThat(processor.bestimmeBereich(kalk), Is.is(Integer.valueOf(2)));
    kalk.advance();
    Assert.assertThat(processor.bestimmeBereich(kalk), Is.is(Integer.valueOf(2)));
    kalk.advance();
    Assert.assertThat(processor.bestimmeBereich(kalk), Is.is(Integer.valueOf(2)));
    kalk.advance();
    Assert.assertThat(processor.bestimmeBereich(kalk), Is.is(Integer.valueOf(2)));
    kalk.advance();
    Assert.assertThat(processor.bestimmeBereich(kalk), Is.is(Integer.valueOf(2)));
    kalk.advance();
  }

  
  @Test(expected = RuntimeException.class)
  public void bestimmeGruppeMitException() {
    MyKalk kalk = new MyKalk(new String[] { "AQuatsch" });
    MasImportItemProcessor processor = new MasImportItemProcessor();
    processor.bestimmeBereich(kalk);
  }
}
