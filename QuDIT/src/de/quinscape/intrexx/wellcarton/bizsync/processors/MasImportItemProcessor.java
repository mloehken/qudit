package de.quinscape.intrexx.wellcarton.bizsync.processors;

import de.quinscape.intrexx.wellcarton.bizsync.domain.Kalkulation;
import java.util.HashMap;
import java.util.Map;
import org.springframework.batch.item.ItemProcessor;




public class MasImportItemProcessor
  extends Object
  implements ItemProcessor<Kalkulation, Map<String, Object>>
{
  public Map<String, Object> process(Kalkulation paramKalkulation) throws Exception {
    HashMap hashMap = new HashMap();
    hashMap.put("KALKULATION_XKEY", paramKalkulation.getKundenKalKey());
    hashMap.put("ARTIKELBEZEICHNUNG", paramKalkulation.getMaterialBezeichnung());
    hashMap.put("MUSTERAUFTRAG_ERSTELLT_AM", paramKalkulation.getAufgabeErstelltAm().toGregorianCalendar().getTime());
    
    hashMap.put("REF_AUFGABENERSTELLER_LOGIN", paramKalkulation.getAufgabeErsteller().trim());
    hashMap.put("REF_SACHBEARBEITER_LOGIN", paramKalkulation.getSachbearbeiter1().trim());
    hashMap.put("REF_SACHBEARBEITER_2_LOGIN", paramKalkulation.getSachbearbeiter2().trim());
    hashMap.put("REF_VERTRETER_DES_KUNDEN_LOGIN", paramKalkulation.getVertreterKunde());
    if ("676".equals(paramKalkulation.getMandant()) && "003".equals(paramKalkulation.getUFirma())) {
      hashMap.put("REF_MANDANT", Integer.valueOf(604));
    } else if ("676".equals(paramKalkulation.getMandant()) && "007".equals(paramKalkulation.getUFirma())) {
      hashMap.put("REF_MANDANT", Integer.valueOf(648));
    } else if ("627".equals(paramKalkulation.getMandant()) && "001".equals(paramKalkulation.getUFirma())) {
      hashMap.put("REF_MANDANT", Integer.valueOf(627));
    } else {
      hashMap.put("REF_MANDANT", Integer.valueOf(0));
    } 
    hashMap.put("KUNDE_KURZBEZEICHNUNG", paramKalkulation.getKundenKurzbezeichnung());
    String str = paramKalkulation.getKalkulationsnummer() + "." + paramKalkulation.getKalkulationsUNummer() + " - " + paramKalkulation.getKundenKurzbezeichnung() + " - " + paramKalkulation.getMaterialBezeichnung();

    
    if (str.length() > 255) {
      str = str.substring(0, 255);
    }
    hashMap.put("STR_KALKNR_KUNDE_ARTIKEL", str.toString());
    hashMap.put("REF_ART_DER_AUFGABE", paramKalkulation.getAufgabeArt());
    hashMap.put("BEMERKUNGEN", paramKalkulation.getAufgabeInformation());
    hashMap.put("FAELLIGKEITSDATUM", paramKalkulation.getFaelligkeitsdatum().toGregorianCalendar().getTime());
    hashMap.put("URSPRUNGSTERMIN_BIS", paramKalkulation.getFaelligkeitsdatum().toGregorianCalendar().getTime());
    
    hashMap.put("FLT_KALKNR", Double.valueOf(paramKalkulation.getKalkulationsnummer()));
    hashMap.put("FLT_KALKUNRDOUBLE", Double.valueOf(paramKalkulation.getKalkulationsUNummer()));
    hashMap.put("ARTIKELGRUPPE", String.valueOf(paramKalkulation.getMaterialGruppe()));
    hashMap.put("KALKULATIONSNUMMER", paramKalkulation.getKalkulationsnummer() + "." + paramKalkulation.getKalkulationsUNummer());
    
    hashMap.put("KUNDENNUMMER", paramKalkulation.getKundenNummer() + "." + paramKalkulation.getKundenUNr());
    
    hashMap.put("KUNDEN_U_NUMMER", paramKalkulation.getKundenUNr());
    hashMap.put("FIRMA", paramKalkulation.getFirma());
    hashMap.put("U_FIRMA", paramKalkulation.getUFirma());
    hashMap.put("KALKULATION_U_NUMMER", Integer.valueOf(paramKalkulation.getKalkulationsUNummer()));
    
    hashMap.put("REF_AUFTRAGSSTATUS", Integer.valueOf(1));
    hashMap.put("PROJEKTNUMMER", paramKalkulation.getProjektnummer());
    hashMap.put("REF_BEREICHVE", bestimmeBereich(paramKalkulation));
    hashMap.put("BREITE", paramKalkulation.getBreite());
    hashMap.put("LAENGE", paramKalkulation.getLaenge());
    hashMap.put("HOEHE", paramKalkulation.getHoehe());
    hashMap.put("KUNDEN_ARTIKEL_BEZEICHNUNG", paramKalkulation.getKundenArtikelBezeichnung());
    hashMap.put("KUNDEN_ARTIKEL_NUMMER", paramKalkulation.getKundenArtikelNummer());
    hashMap.put("STATUS", paramKalkulation.getStatus());
    hashMap.put("STATUS_BEZEICHNUNG", paramKalkulation.getStatusBezeichnung());
    hashMap.put("BEZEICHNUNG_XML", paramKalkulation.getBezeichnung());
    hashMap.put("CAD_NUMMER", paramKalkulation.getCadNummer());
    hashMap.put("MENGEN_FAKTOR", paramKalkulation.getMengenFaktor());
    hashMap.put("TEILENUMMER", paramKalkulation.getTeilenummer());
    hashMap.put("FEFCO_NUMMER", paramKalkulation.getFefcoNummer());
    hashMap.put("VERNUTZUNG", paramKalkulation.getVernutzung());
    hashMap.put("BREITEN_ZUSCHNITT", paramKalkulation.getBreitenZuschnitt());
    hashMap.put("LAENGEN_ZUSCHNITT", paramKalkulation.getLaengenZuschnitt());
    hashMap.put("BZ_LZ_MANUELL", paramKalkulation.getBzLzManuell());
    hashMap.put("NUTZEN_BREITE", paramKalkulation.getNutzenBreite());
    hashMap.put("NUTZEN_LAENGE", paramKalkulation.getNutzenLaenge());
    hashMap.put("BREITEN_ZUSCHNITT_VERNUTZT", paramKalkulation.getBreitenZuschnittVernutzt());
    hashMap.put("LAENGEN_ZUSCHNITT_VERNUTZT", paramKalkulation.getLaengenZuschnittVernutzt());
    hashMap.put("BREITEN_ZUSCHNITT_STANZRAND", paramKalkulation.getBreitenZuschnittStanzrand());
    hashMap.put("LAENGEN_ZUSCHNITT_STANZRAND", paramKalkulation.getLaengenZuschnittStanzrand());
    hashMap.put("SORTE_NUMMER", paramKalkulation.getSorteNummer());
    hashMap.put("INAKTIV", paramKalkulation.getInaktiv());
    hashMap.put("MATERIAL_TYP", paramKalkulation.getMaterialTyp());
    hashMap.put("MATERIAL_NUMMER", paramKalkulation.getMaterialNummer());
    hashMap.put("VERLADE_BREITE", paramKalkulation.getVerladeBreite());
    hashMap.put("VERLADE_LAENGE", paramKalkulation.getVerladeLaenge());
    hashMap.put("VERLADE_HOEHE", paramKalkulation.getVerladeHoehe());
    hashMap.put("VERLADE_MANUELL", paramKalkulation.getVerladeManuell());
    hashMap.put("BZ_RILLER", paramKalkulation.getBzRiller());
    hashMap.put("LZ_RILLER", paramKalkulation.getLzRiller());
    hashMap.put("BZ_SCHLITZER", paramKalkulation.getBzSchlitzer());
    hashMap.put("LZ_SCHLITZER", paramKalkulation.getLzSchlitzer());
    hashMap.put("ABFALL_NETTO_BOGEN", paramKalkulation.getAbfallNettoBogen());
    hashMap.put("KB_A_SORTE_NUMMER", paramKalkulation.getKbASorteNummer());
    hashMap.put("KB_A_BREITEN_ZUSCHNITT", paramKalkulation.getKbABreitenZuschnitt());
    hashMap.put("KB_A_LAENGEN_ZUSCHNITT", paramKalkulation.getKbALaengenZuschnitt());
    hashMap.put("KB_A_BREITE_ZUSCHNITT_RAND", paramKalkulation.getKbABreiteZuschnittRand());
    hashMap.put("KB_A_LAENGE_ZUSCHNITT_RAND", paramKalkulation.getKbALaengeZuschnittRand());
    hashMap.put("KB_B_SORTE_NUMMER", paramKalkulation.getKbBSorteNummer());
    hashMap.put("KB_B_BREITEN_ZUSCHNITT", paramKalkulation.getKbBBreitenZuschnitt());
    hashMap.put("KB_B_LAENGEN_ZUSCHNITT", paramKalkulation.getKbBLaengenZuschnitt());
    hashMap.put("KB_B_BREITE_ZUSCHNITT_RAND", paramKalkulation.getKbBBreiteZuschnittRand());
    hashMap.put("KB_B_LAENGE_ZUSCHNITT_RAND", paramKalkulation.getKbBLaengeZuschnittRand());
    hashMap.put("LEGEVARIANTE_NUMMER", paramKalkulation.getLegevarianteNummer());
    hashMap.put("DRUCK_FF_NUMMER", paramKalkulation.getDruckFfNummer());
    hashMap.put("DRUCK_FF_ART", paramKalkulation.getDruckFfArt());
    hashMap.put("DRUCK_FF_WOMIT", paramKalkulation.getDruckFfWomit());
    hashMap.put("VER_FF_NUMMER", paramKalkulation.getVerFfNummer());
    hashMap.put("VER_FF_ART", paramKalkulation.getVerFfArt());
    hashMap.put("VER_FF_WOMIT", paramKalkulation.getVerFfWomit());
    hashMap.put("STANZEN_FF_NUMMER", paramKalkulation.getStanzenFfNummer());
    hashMap.put("STANZEN_FF_ART", paramKalkulation.getStanzenFfArt());
    hashMap.put("STANZEN_FF_WOMIT", paramKalkulation.getStanzenFfWomit());
    hashMap.put("KASCH_FF_NUMMER", paramKalkulation.getKaschFfNummer());
    hashMap.put("KASCH_FF_ART", paramKalkulation.getKaschFfArt());
    hashMap.put("KASCH_FF_WOMIT", paramKalkulation.getKaschFfWomit());
    hashMap.put("KAL_KEY", paramKalkulation.getKalKey());
    hashMap.put("UNIQUE_ID", paramKalkulation.getUniqueId());
    return hashMap;
  }

  
  Integer bestimmeBereich(Kalkulation paramKalkulation) {
    String str1 = String.valueOf(paramKalkulation.getMaterialGruppe());
    String str2 = " " + str1 + ",";
    if (" A71, A73, A74, A76, ABR,".contains(str2)) {
      return new Integer(true);
    }
    if (" A31, A34, ABU,".contains(str2)) {
      return new Integer(2);
    }
    if (" A33, A36, A39, ADI,".contains(str2)) {
      return new Integer(3);
    }
    if (" LOH,".contains(str2)) {
      return new Integer(4);
    }
    if (" ADV,".contains(str2)) {
      return new Integer(5);
    }
    
    throw new RuntimeException("Kalkulation enthaelt unbekannte Materialgruppe '" + str1 + "'; es konnte kein Bereich dazu bestimmt werden.");
  }
}
