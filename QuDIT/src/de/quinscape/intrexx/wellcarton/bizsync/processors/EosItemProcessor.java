package de.quinscape.intrexx.wellcarton.bizsync.processors;

import java.util.Map;
import org.springframework.batch.item.ItemProcessor;




















public class EosItemProcessor
  extends Object
  implements ItemProcessor<Map<String, Object>, Map<String, Object>>
{
  public Map<String, Object> process(Map<String, Object> item) throws Exception {
    if ("Leer".equals(item.get("KD_ORDER_NR")) || noInteger(item.get("KD_ORDER_NR")))
      item.put("KD_ORDER_NR", null); 
    return item;
  }









  
  private static boolean noInteger(Object object) {
    if (object == null)
      return false; 
    String string = object.toString();
    if (string.length() == 0)
      return false; 
    return (string.replaceFirst("[0-9]+", "").length() == 0);
  }
}
