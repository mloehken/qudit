package de.quinscape.intrexx.wellcarton.bizsync.processors;

import de.quinscape.intrexx.wellcarton.bizsync.domain.Kalkulation;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import javax.xml.datatype.DatatypeFactory;
import org.springframework.batch.item.ItemProcessor;



















public class MasExportProcessor
  extends Object
  implements ItemProcessor<Map<String, Object>, Kalkulation>
{
  private String clobToString(Clob clob) throws SQLException {
    if (clob == null) return null; 
    return clob.getSubString(1L, (int)clob.length());
  }



  
  public Kalkulation process(Map<String, Object> item) throws Exception {
    Kalkulation kalk = new Kalkulation();
    
    kalk.setKalkulationsnummer(((Double)item.get("FLT_KALKNR")).intValue());
    kalk.setKalkulationsUNummer(((Double)item.get("FLT_KALKUNRDOUBLE")).intValue());

    
    kalk.setKundenKalKey(integer(item, "KALKULATION_XKEY"));
    kalk.setAufgabeArt(integer(item, "REF_ART_DER_AUFGABE"));
    kalk.setTeilenummer(integer(item, "TEILENUMMER"));
    kalk.setBzLzManuell(integer(item, "BZ_LZ_MANUELL"));
    kalk.setVernutzung(integer(item, "VERNUTZUNG"));
    kalk.setInaktiv(integer(item, "INAKTIV"));
    kalk.setVerladeManuell(integer(item, "VERLADE_MANUELL"));
    kalk.setKalKey(integer(item, "KAL_KEY"));
    kalk.setUniqueId(integer(item, "UNIQUE_ID"));

    
    kalk.setProjektnummer(item.get("PROJEKTNUMMER").toString());
    Integer mandant = (Integer)item.get("REF_MANDANT");
    if (mandant.intValue() == 604) {
      
      kalk.setMandant("676");
      kalk.setUFirma("003");
    }
    else if (mandant.intValue() == 648) {
      
      kalk.setMandant("676");
      kalk.setUFirma("007");
    }
    else if (mandant.intValue() == 627) {
      
      kalk.setMandant("627");
      kalk.setUFirma("001");
    } 
    kalk.setFirma("000");
    String[] kundenNummer = item.get("KUNDENNUMMER").toString().split("\\.");
    kalk.setKundenNummer(kundenNummer[0]);
    kalk.setKundenUNr(kundenNummer[1]);
    kalk.setKundenKurzbezeichnung(valueOrEmptyString(item.get("KUNDE_KURZBEZEICHNUNG")));
    kalk.setMaterialBezeichnung(valueOrEmptyString(item.get("ARTIKELBEZEICHNUNG")));
    kalk.setAufgabeErsteller(valueOrEmptyString(item.get("REF_AUFGABENERSTELLER_LOGIN")));
    kalk.setAufgabeSachbearbeiter(valueOrEmptyString(item.get("REF_SACHBEARBEITER_LOGIN")));
    kalk.setVertreterKunde(valueOrEmptyString(item.get("REF_VERTRETER_DES_KUNDEN_LOGIN")));
    kalk.setFefcoNummer(valueOrEmptyString(item.get("FEFCO_NUMMER")));
    kalk.setSorteNummer(valueOrEmptyString(item.get("SORTE_NUMMER")));
    kalk.setMaterialTyp(valueOrEmptyString(item.get("MATERIAL_TYP")));
    kalk.setMaterialNummer(valueOrEmptyString(item.get("MATERIAL_NUMMER")));
    kalk.setBzRiller(valueOrEmptyString(item.get("BZ_RILLER")));
    kalk.setLzRiller(valueOrEmptyString(item.get("LZ_RILLER")));
    kalk.setBzSchlitzer(valueOrEmptyString(item.get("BZ_SCHLITZER")));
    kalk.setBzSchlitzer(valueOrEmptyString(item.get("LZ_SCHLITZER")));
    kalk.setKbASorteNummer(valueOrEmptyString(item.get("KB_A_SORTE_NUMMER")));
    kalk.setKbBSorteNummer(valueOrEmptyString(item.get("KB_B_SORTE_NUMMER")));
    kalk.setLegevarianteNummer(valueOrEmptyString(item.get("LEGEVARIANTE_NUMMER")));
    kalk.setDruckFfNummer(valueOrEmptyString(item.get("DRUCK_FF_NUMMER")));
    kalk.setDruckFfArt(valueOrEmptyString(item.get("DRUCK_FF_ART")));
    kalk.setDruckFfWomit(valueOrEmptyString(item.get("DRUCK_FF_WOMIT")));
    kalk.setVerFfNummer(valueOrEmptyString(item.get("VER_FF_NUMMER")));
    kalk.setVerFfArt(valueOrEmptyString(item.get("VER_FF_ART")));
    kalk.setVerFfWomit(valueOrEmptyString(item.get("VER_FF_WOMIT")));
    kalk.setStanzenFfNummer(valueOrEmptyString(item.get("STANZEN_FF_NUMMER")));
    kalk.setStanzenFfArt(valueOrEmptyString(item.get("STANZEN_FF_ART")));
    kalk.setStanzenFfWomit(valueOrEmptyString(item.get("STANZEN_FF_WOMIT")));
    kalk.setKaschFfNummer(valueOrEmptyString(item.get("KASCH_FF_NUMMER")));
    kalk.setKaschFfArt(valueOrEmptyString(item.get("KASCH_FF_ART")));
    kalk.setKaschFfWomit(valueOrEmptyString(item.get("KASCH_FF_WOMIT")));
    kalk.setStatus(valueOrEmptyString(item.get("STATUS")));
    kalk.setMaterialGruppe(valueOrEmptyString(item.get("ARTIKELGRUPPE")));
    kalk.setKundenArtikelBezeichnung(valueOrEmptyString(item.get("KUNDEN_ARTIKEL_BEZEICHNUNG")));
    kalk.setKundenArtikelNummer(valueOrEmptyString(item.get("KUNDEN_ARTIKEL_NUMMER")));
    kalk.setBezeichnung(valueOrEmptyString(item.get("BEZEICHNUNG_XML")));
    kalk.setSachbearbeiter1(valueOrEmptyString(item.get("REF_SACHBEARBEITER_LOGIN")));
    kalk.setSachbearbeiter2(valueOrEmptyString(item.get("REF_SACHBEARBEITER_2_LOGIN")));
    kalk.setStatusBezeichnung(valueOrEmptyString(item.get("STATUS_BEZEICHNUNG")));
    kalk.setCadNummer(valueOrEmptyString(item.get("CAD_NUMMER")));

    
    GregorianCalendar erstelltAm = new GregorianCalendar();
    erstelltAm.setTime((Date)item.get("MUSTERAUFTRAG_ERSTELLT_AM"));
    kalk.setAufgabeErstelltAm(DatatypeFactory.newInstance().newXMLGregorianCalendar(erstelltAm));
    GregorianCalendar faelligkeitsdatum = new GregorianCalendar();
    faelligkeitsdatum.setTime((Date)item.get("FAELLIGKEITSDATUM"));
    kalk.setFaelligkeitsdatum(DatatypeFactory.newInstance().newXMLGregorianCalendar(
          faelligkeitsdatum));

    
    kalk.setAufgabeInformation(clobToString((Clob)item.get("BEMERKUNGEN")));

    
    kalk.setMengenFaktor((BigDecimal)item.get("MENGEN_FAKTOR"));
    kalk.setBreite((BigDecimal)item.get("BREITE"));
    kalk.setLaenge((BigDecimal)item.get("LAENGE"));
    kalk.setHoehe((BigDecimal)item.get("HOEHE"));
    kalk.setBreitenZuschnitt((BigDecimal)item.get("BREITEN_ZUSCHNITT"));
    kalk.setLaengenZuschnitt((BigDecimal)item.get("LAENGEN_ZUSCHNITT"));
    kalk.setNutzenBreite((BigDecimal)item.get("NUTZEN_BREITE"));
    kalk.setNutzenLaenge((BigDecimal)item.get("NUTZEN_LAENGE"));
    kalk.setBreitenZuschnittVernutzt((BigDecimal)item.get("BREITEN_ZUSCHNITT_VERNUTZT"));
    kalk.setLaengenZuschnittVernutzt((BigDecimal)item.get("LAENGEN_ZUSCHNITT_VERNUTZT"));
    kalk.setVerladeBreite((BigDecimal)item.get("VERLADE_BREITE"));
    kalk.setVerladeLaenge((BigDecimal)item.get("VERLADE_LAENGE"));
    kalk.setVerladeHoehe((BigDecimal)item.get("VERLADE_HOEHE"));
    kalk.setAbfallNettoBogen((BigDecimal)item.get("ABFALL_NETTO_BOGEN"));
    kalk.setKbABreitenZuschnitt((BigDecimal)item.get("KB_A_BREITEN_ZUSCHNITT"));
    kalk.setKbALaengenZuschnitt((BigDecimal)item.get("KB_A_LAENGEN_ZUSCHNITT"));
    kalk.setKbABreiteZuschnittRand((BigDecimal)item.get("KB_A_BREITE_ZUSCHNITT_RAND"));
    kalk.setKbALaengeZuschnittRand((BigDecimal)item.get("KB_A_LAENGE_ZUSCHNITT_RAND"));
    kalk.setKbBBreitenZuschnitt((BigDecimal)item.get("KB_B_BREITEN_ZUSCHNITT"));
    kalk.setKbBLaengenZuschnitt((BigDecimal)item.get("KB_B_LAENGEN_ZUSCHNITT"));
    kalk.setKbABreiteZuschnittRand((BigDecimal)item.get("KB_B_BREITE_ZUSCHNITT_RAND"));
    kalk.setKbBLaengeZuschnittRand((BigDecimal)item.get("KB_B_LAENGE_ZUSCHNITT_RAND"));
    kalk.setBreitenZuschnittStanzrand((BigDecimal)item.get("BREITEN_ZUSCHNITT_STANZRAND"));
    kalk.setLaengenZuschnittStanzrand((BigDecimal)item.get("LAENGEN_ZUSCHNITT_STANZRAND"));
    
    return kalk;
  }









  
  private Integer integer(Map<String, Object> map, String key) { return (Integer)map.get(key); }










  
  private String valueOrEmptyString(Object object) {
    if (object != null)
      return (object.toString().length() > 0) ? object.toString() : ""; 
    return "";
  }
}
