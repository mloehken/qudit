package de.quinscape.intrexx.wellcarton.bizsync;

import de.quinscape.intrexx.db.tester.AbstractSpringBatchTest;
import de.quinscape.intrexx.db.tester.annotations.InitialDataSets;
import de.quinscape.intrexx.db.tester.annotations.SpringContext;
import org.junit.Test;







@SpringContext({"cfg/spring/context.xml", "cfg/spring/db.xml"})
public class Mas2ExportTest
  extends AbstractSpringBatchTest
{
  @Test
  @InitialDataSets({"mas2Export-initialData"})
  public void mas2Export() { runJob("mas2Export"); }
}
